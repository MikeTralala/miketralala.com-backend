<?php

declare(strict_types=1);

namespace App\Tracker;

use Symfony\Component\Uid\Uuid;

class UidTrackerIdGenerator implements TrackerIdGeneratorInterface
{
    public function generate(): string
    {
        return (string)Uuid::v4();
    }
}
