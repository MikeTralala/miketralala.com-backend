<?php

declare(strict_types=1);

namespace App\Tracker;

interface TrackerVaultInterface
{
    public function getTrackingId(): ?string;

    public function setTrackerId(string $trackingId): void;

    public function overrideTrackerId(string $trackingId): void;
}
