<?php

declare(strict_types=1);

namespace App\Feature\Mail\Event\Handler;

use App\Feature\Mail\Command\SendUserActivationMail;
use App\Feature\User\Event\UserActivated;
use App\Messenger\Message\Handler\EventHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendUserActivatedMailWhenUserActivated implements EventHandlerInterface
{
    public function __construct(
        private MessageBusInterface $commandBus
    ) {
    }

    public function __invoke(UserActivated $event)
    {
        $this->commandBus->dispatch(new SendUserActivationMail($event->user->getId()));
    }
}
