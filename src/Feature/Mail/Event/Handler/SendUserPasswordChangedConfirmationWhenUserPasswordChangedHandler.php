<?php

declare(strict_types=1);

namespace App\Feature\Mail\Event\Handler;

use App\Feature\Mail\Command\SendUserPasswordChangedConfirmation;
use App\Feature\User\Event\UserPasswordChanged;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendUserPasswordChangedConfirmationWhenUserPasswordChangedHandler
{
    public function __construct(
        private MessageBusInterface $commandBus
    ) { }


    public function __invoke(UserPasswordChanged $event)
    {
        $this->commandBus->dispatch(new SendUserPasswordChangedConfirmation($event->user->getId()));
    }
}
