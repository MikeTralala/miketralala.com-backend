<?php

declare(strict_types=1);

namespace App\Feature\Mail\Event\Handler;

use App\Feature\Mail\Command\SendUserActivatedMail;
use App\Feature\Mail\Command\SendUserActivationMail;
use App\Feature\User\Event\UserRegistered;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\EventHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendUserActivationMailWhenUserRegistered implements EventHandlerInterface
{
    public function __construct(
        private MessageBusInterface $commandBus,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(UserRegistered $event)
    {
        $user = $this->userRepository->mustFind($event->user->getId());

        if ($user->isActive()) {
            $this->commandBus->dispatch(new SendUserActivatedMail($event->user->getId()));

            return;
        }

        $this->commandBus->dispatch(new SendUserActivationMail($event->user->getId()));
    }
}
