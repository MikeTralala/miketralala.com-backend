<?php

declare(strict_types=1);

namespace App\Feature\Mail\Command;

use App\Messenger\Message\AsynchronousMessageInterface;

final class SendMail implements AsynchronousMessageInterface
{
    public const DEFAULT_FROM = 'noreply@miketralala.com';

    public function __construct(
        public readonly string $recipients,
        public readonly string $subject,
        public readonly string $content,
        public readonly string $from = self::DEFAULT_FROM
    ) {
    }
}
