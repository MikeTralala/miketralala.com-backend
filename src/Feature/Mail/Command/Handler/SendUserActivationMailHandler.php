<?php

declare(strict_types=1);

namespace App\Feature\Mail\Command\Handler;

use App\Feature\Mail\Command\SendMail;
use App\Feature\Mail\Command\SendUserActivationMail;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendUserActivationMailHandler implements CommandHandlerInterface
{
    public function __construct(
        private MessageBusInterface $commandBus,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(SendUserActivationMail $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $this->commandBus->dispatch(
            new SendMail(
                $user->getEmailAddress(),
                'Activate your account!',
                'Please click HERE to activate your account. please do so within the next 24 hours!'
            )
        );
    }
}
