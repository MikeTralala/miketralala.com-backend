<?php

declare(strict_types=1);

namespace App\Feature\Mail\Command\Handler;

use App\Feature\Mail\Command\SendMail;
use App\Feature\Mail\Command\SendUserPasswordChangedConfirmation;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendUserPasswordChangedConfirmationHandler implements CommandHandlerInterface
{

    public function __construct(
        private MessageBusInterface $commandBus,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(SendUserPasswordChangedConfirmation $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $this->commandBus->dispatch(
            new SendMail(
                $user->getEmailAddress(),
                'Your password has been changed',
                'Your password has been successfully changed, if this was nog you please recover your account with this <link>'
            )
        );
    }
}
