<?php

declare(strict_types=1);

namespace App\Feature\Mail\Command\Handler;

use App\Feature\Mail\Command\SendMail;
use App\Messenger\Message\Handler\CommandHandlerInterface;

final class SendMailHandler implements CommandHandlerInterface
{
    public function __invoke(SendMail $command): void
    {
        // TODO: write mailer implementation
    }
}
