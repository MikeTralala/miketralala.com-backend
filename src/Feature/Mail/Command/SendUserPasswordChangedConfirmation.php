<?php

declare(strict_types=1);

namespace App\Feature\Mail\Command;

use App\Messenger\Message\AsynchronousMessageInterface;

class SendUserPasswordChangedConfirmation implements AsynchronousMessageInterface
{
    public function __construct(
        public readonly string $userId
    ) {
    }
}
