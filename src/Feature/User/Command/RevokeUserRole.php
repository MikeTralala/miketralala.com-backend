<?php

declare(strict_types=1);

namespace App\Feature\User\Command;

use App\Feature\User\Entity\Role;
use Symfony\Component\Validator\Constraints as Assert;

final class RevokeUserRole
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
        #[Assert\NotBlank]
        public readonly int $roleId
    ) {
    }
}
