<?php

declare(strict_types=1);

namespace App\Feature\User\Command;

use Rollerworks\Component\PasswordCommonList\Constraints\NotInPasswordCommonList;
use Symfony\Component\Validator\Constraints as Assert;

final class RegisterUser
{
    public function __construct(
        #[Assert\Email(mode: Assert\Email::VALIDATION_MODE_HTML5)]
        #[Assert\NotBlank]
        public readonly string $email,
        #[Assert\NotBlank]
        #[Assert\Length(min: 3, max: 120)]
        public readonly string $name,
        #[Assert\NotBlank]
        #[NotInPasswordCommonList]
        public readonly string $password,
        public readonly bool $isAuthor,
        public readonly bool $activateAtRegistration = false
    ) {
    }
}
