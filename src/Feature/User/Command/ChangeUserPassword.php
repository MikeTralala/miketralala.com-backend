<?php

declare(strict_types=1);

namespace App\Feature\User\Command;

use Rollerworks\Component\PasswordCommonList\Constraints\NotInPasswordCommonList;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

final class ChangeUserPassword
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
        #[Assert\NotBlank]
        #[SecurityAssert\UserPassword(message: 'Invalid password')]
        public readonly string $originalPassword,
        #[Assert\NotBlank]
        #[NotInPasswordCommonList]
        public readonly string $password
    ) {
    }
}
