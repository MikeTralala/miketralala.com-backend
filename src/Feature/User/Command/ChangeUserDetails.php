<?php

declare(strict_types=1);

namespace App\Feature\User\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class ChangeUserDetails
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
        #[Assert\Email(mode: Assert\Email::VALIDATION_MODE_HTML5)]
        #[Assert\NotBlank]
        public readonly string $emailAddress,
        #[Assert\NotBlank]
        #[Assert\Length(min: 3, max: 120)]
        public readonly string $name,
    ) {
    }
}
