<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\Feature\User\Command\ChangeUserPassword;
use App\Feature\User\Event\UserPasswordChanged;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class ChangeUserPasswordHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private UserPasswordHasherInterface $passwordHasher,
        private MessageBusInterface $eventBus,
    ) {
    }

    public function __invoke(ChangeUserPassword $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $command->password)
        );

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserPasswordChanged($user));
    }
}
