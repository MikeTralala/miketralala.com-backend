<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\Feature\User\Command\ChangeUserDetails;
use App\Feature\User\Event\UserDetailsChanged;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ChangeUserDetailsHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageBusInterface $eventBus
    ) {
    }

    public function __invoke(ChangeUserDetails $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $changes = [];
        if ($user->getName() !== $command->name) {
            $user->changeName($command->name);
            $changes[] = UserDetailsChanged::NAME;
        }

        if ($user->getEmailAddress() !== $command->emailAddress) {
            $user->changeEmailAddress($command->emailAddress);
            $changes[] = UserDetailsChanged::EMAIL_ADDRESS;
        }

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserDetailsChanged($user, $changes), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
