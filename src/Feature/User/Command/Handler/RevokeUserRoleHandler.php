<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\Exception\AppRuntimeException;
use App\Feature\User\Command\RevokeUserRole;
use App\Feature\User\Entity\User;
use App\Feature\User\Event\UserRoleRevoked;
use App\Feature\User\Repository\RoleRepository;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class RevokeUserRoleHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private RoleRepository $roleRepository,
        private MessageBusInterface $eventBus
    ) {
    }

    public function __invoke(RevokeUserRole $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);
        $role = $this->roleRepository->mustFind($command->roleId);

        if (! $user->hasRole($role)) {
            AppRuntimeException::alreadyExists(User::class, 'role', $role->getName());
        }

        $user->removeRole($role);

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserRoleRevoked($user, $role), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
