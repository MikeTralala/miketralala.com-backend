<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\User\Action\UserDeactivation;
use App\Feature\User\Command\DeactivateUser;
use App\Feature\User\Event\UserDeactivated;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class DeactivateUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(DeactivateUser $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new UserDeactivation($user));

        $user->deactivate();

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserDeactivated($user), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
