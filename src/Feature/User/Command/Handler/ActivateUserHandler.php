<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\User\Action\UserActivation;
use App\Feature\User\Command\ActivateUser;
use App\Feature\User\Event\UserActivated;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ActivateUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(ActivateUser $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new UserActivation($user));

        $user->activate();
        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserActivated($user), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
