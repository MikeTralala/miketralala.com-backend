<?php

declare(strict_types=1);

namespace App\Feature\User\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\User\Action\UserRegistration;
use App\Feature\User\Command\RegisterUser;
use App\Feature\User\Entity\Role;
use App\Feature\User\Entity\User;
use App\Feature\User\Event\UserRegistered;
use App\Feature\User\Repository\RoleRepository;
use App\Feature\User\Repository\UserRepository;
use App\Messenger\Message\Handler\CommandHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class RegisterUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private RoleRepository $roleRepository,
        private UserPasswordHasherInterface $passwordHasher,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(RegisterUser $command): void
    {
        $this->guardian->protect(new UserRegistration($command->email));

        $roles = [$this->roleRepository->mustFind(Role::USER)];
        if ($command->isAuthor) {
            $roles[] = $this->roleRepository->mustFind(Role::AUTHOR);
        }

        $user = new User(
            $command->email,
            $command->name,
            $roles
        );

        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $command->password)
        );

        if ($command->activateAtRegistration) {
            $user->activate();
        }

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserRegistered($user), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
