<?php

declare(strict_types=1);

namespace App\Feature\User\Entity;

use App\Entity\Traits\HasComparableId;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Role
{
    use HasComparableId;

    public const USER          = 1;
    public const AUTHOR        = 2;
    public const MODERATOR     = 3;
    public const ADMINISTRATOR = 4;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', nullable: false)]
    private int $id;

    #[ORM\Column(type: 'string', nullable: false)]
    private string $name;

    public function __construct(
        int $id,
        string $name
    ) {
        $this->id   = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return sprintf('ROLE_%s', strtoupper($this->name));
    }
}
