<?php

declare(strict_types=1);

namespace App\Feature\User\Entity;

use App\Entity\Traits\HasComparableId;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use HasComparableId;

    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private string $emailAddress;

    #[ORM\Column(type: 'string', length: 180, unique: false)]
    private string $name;

    #[ORM\OneToMany(targetEntity: Role::class)]
    private Collection $roles;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $activatedAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $deactivatedAt;

    #[ORM\Column(type: 'string', nullable: false)]
    private string $password;

    public function __construct(
        string $email,
        string $name,
        array $roles = []
    ) {
        $this->id           = Ulid::generate();
        $this->emailAddress = $email;
        $this->name         = $name;
        $this->roles        = new ArrayCollection($roles);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function isActive(): bool
    {
        return $this->activatedAt !== null;
    }

    public function activate(): void
    {
        $this->deactivatedAt = null;
        $this->activatedAt   = new DateTimeImmutable();
    }

    public function deactivate(): void
    {
        $this->activatedAt   = null;
        $this->deactivatedAt = new DateTimeImmutable();
    }

    #[Pure] public function getUserIdentifier(): string
    {
        return $this->getEmailAddress();
    }

    public function getRoles(): array
    {
        return $this->roles->toArray();
    }

    public function hasRole(Role|int|array $role): bool
    {
        return $this->roles->exists(function (Role $userRole) use ($role) {
            return $userRole->is($role);
        });
    }

    public function addRole(Role $role): void
    {
        $this->roles->add($role);
    }

    public function removeRole(Role $role): void
    {
        $this->roles->removeElement($role);
    }

    public function eraseCredentials(): void
    {
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function __toString(): string
    {
        dump(debug_backtrace());

        return $this->id;
    }

    public function changeName(string $name): void
    {
        $this->name = $name;
    }

    public function changeEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }
}
