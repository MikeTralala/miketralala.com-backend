<?php

declare(strict_types=1);

namespace App\Feature\User\Action;


use App\Feature\User\Entity\User;

final class UserActivation
{
    public function __construct(
        public readonly User $user
    ) {
    }
}
