<?php

declare(strict_types=1);

namespace App\Feature\User\Action;

final class UserRegistration
{
    public function __construct(
        public readonly string $emailAddress
    ) {
    }
}
