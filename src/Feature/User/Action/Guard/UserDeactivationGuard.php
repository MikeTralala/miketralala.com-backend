<?php

declare(strict_types=1);

namespace App\Feature\User\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\Feature\User\Action\UserActivation;
use App\ActionGuard\FeedbackInterface;

final class UserDeactivationGuard implements ActionGuardInterface
{
    public function __invoke(
        UserActivation $intent,
        FeedbackInterface $feedback
    ): void {
        if (! $intent->user->isActive()) {
            $feedback->denyBecause('User is already inactive');
        }
    }
}
