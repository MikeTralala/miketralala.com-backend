<?php

declare(strict_types=1);

namespace App\Feature\User\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\User\Action\UserRegistration;
use App\Feature\User\Repository\UserRepository;

final class UserRegistrationGuard implements ActionGuardInterface
{
    public function __construct(
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(
        UserRegistration $intent,
        FeedbackInterface $feedback
    ): void {
        if ($this->userRepository->findByEmail($intent->emailAddress)) {
            $feedback->denyBecause('User with the same email address already exists');
        }
    }
}
