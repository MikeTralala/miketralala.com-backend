<?php

declare(strict_types=1);

namespace App\Feature\User\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\Feature\User\Action\UserActivation;
use App\ActionGuard\FeedbackInterface;

final class UserActivationGuard implements ActionGuardInterface
{
    public function __invoke(
        UserActivation $action,
        FeedbackInterface $feedback
    ): void {
        if ($action->user->isActive()) {
            $feedback->denyBecause('User is already active');
        }
    }
}
