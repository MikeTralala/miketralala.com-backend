<?php

declare(strict_types=1);

namespace App\Feature\User\Event;

use App\Feature\User\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

final class UserRegistered
{
    public function __construct(
        public readonly User $user,
    ) {
    }
}
