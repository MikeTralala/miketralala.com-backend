<?php

declare(strict_types=1);

namespace App\Feature\User\Event;

use App\Feature\User\Entity\User;

final class UserDetailsChanged
{
    public const EMAIL_ADDRESS = 'email';
    public const NAME          = 'name';

    public function __construct(
        public readonly User $user,
        public readonly array $changedFields
    ) {
    }
}
