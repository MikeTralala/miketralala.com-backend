<?php

declare(strict_types=1);

namespace App\Feature\User\Event;

use App\Feature\User\Entity\User;

final class UserActivated
{
    public function __construct(
        public readonly User $user
    ) {
    }
}
