<?php

namespace App\Feature\User\Repository;

use App\Feature\User\Entity\User;
use App\Exception\AppRuntimeException;
use App\Repository\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: User::class
        );
    }

    public function mustFind(string $id): User
    {
        $user = $this->find($id);
        if (null === $user) {
            throw AppRuntimeException::notFound(User::class, $id);
        }

        return $user;
    }

    public function findByEmail(string $email): ?User
    {
        $query = $this->createQueryBuilder('user')
                      ->where('user.email = :email')
                      ->getQuery();

        return $query
            ->setParameter('email', $email)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function findActive(): array
    {
        $query = $this->createQueryBuilder('user')
                      ->where('user.activatedAt is not null')
                      ->getQuery();

        return $query->getResult();
    }

    public function upgradePassword(
        PasswordAuthenticatedUserInterface $user,
        string $newHashedPassword
    ): void {
        if (! $user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
}
