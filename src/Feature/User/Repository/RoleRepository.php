<?php

declare(strict_types=1);

namespace App\Feature\User\Repository;

use App\Exception\AppRuntimeException;
use App\Feature\User\Entity\Role;
use App\Repository\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: Role::class
        );
    }

    public function mustFind(int $id): Role
    {
        $user = $this->find($id);
        if (null === $user) {
            throw AppRuntimeException::notFound(Role::class, $id);
        }

        return $user;
    }
}
