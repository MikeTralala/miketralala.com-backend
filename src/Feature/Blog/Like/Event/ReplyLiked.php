<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Event;

use App\Feature\Blog\Like\Entity\Like;
use App\Feature\Blog\Reply\Entity\Reply;

final class ReplyLiked
{
    public function __construct(
        public readonly Like $like,
        public readonly Reply $reply
    ) {
    }
}
