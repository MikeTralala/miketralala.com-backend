<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Event;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\User\Entity\User;

final class PostUnliked
{
    public function __construct(
        public readonly string $likeId,
        public readonly Post $post
    ) {
    }
}
