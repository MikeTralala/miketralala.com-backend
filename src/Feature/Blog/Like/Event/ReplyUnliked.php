<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Event;

use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\User\Entity\User;

final class ReplyUnliked
{
    public function __construct(
        public readonly string $likeId,
        public readonly Reply $reply
    ) {
    }
}
