<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Repository;

use App\Exception\AppRuntimeException;
use App\Feature\Blog\Like\Entity\Like;
use App\Repository\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Like|null find($id, $lockMode = null, $lockVersion = null)
 * @method Like|null findOneBy(array $criteria, array $orderBy = null)
 * @method Like[]    findAll()
 * @method Like[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: Like::class
        );
    }

    public function mustFind(string $id): Like
    {
        $reply = $this->find($id);
        if (null === $reply) {
            throw AppRuntimeException::notFound(Like::class, $id);
        }

        return $reply;
    }
}
