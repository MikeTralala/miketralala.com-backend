<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Entity;

use App\Feature\User\Entity\User;
use App\Feature\Blog\Post\Entity\Post;
use App\Feature\Blog\Reply\Entity\Reply;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
class Like
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $by;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\ManyToOne(targetEntity: Post::class, inversedBy: 'likes')]
    private ?Post $post;

    #[ORM\ManyToOne(targetEntity: Reply::class, inversedBy: 'likes')]
    private ?Reply $reply;

    public function __construct(string $id, User $by, DateTimeInterface $at)
    {
        $this->id = $id;
        $this->by = $by;
        $this->at = $at;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeInterface
    {
        return $this->at;
    }

    public function getBy(): User
    {
        return $this->by;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(Post $post): void
    {
        $this->post = $post;
    }

    public function getReply(): ?Reply
    {
        return $this->reply;
    }

    public function setReply(Reply $reply): void
    {
        $this->reply = $reply;
    }
}
