<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Like\Action\ReplyUnliking;

final class ReplyUnlikingGuard implements ActionGuardInterface
{
    public function __invoke(ReplyUnliking $action, FeedbackInterface $feedback): void
    {
        $reply = $action->reply;
        if ($reply->isLikedBy($action->user)) {
            $feedback->denyBecause('Reply is not liked by user');
        }
    }
}
