<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Like\Action\PostUnliking;

final class PostUnlikingGuard implements ActionGuardInterface
{
    public function __invoke(PostUnliking $action, FeedbackInterface $feedback): void
    {
        $post = $action->post;
        if (! $post->isLikedBy($action->user)) {
            $feedback->denyBecause('Post is not liked by user');
        }
    }
}
