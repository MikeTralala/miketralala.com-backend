<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Like\Action\PostLiking;
use App\Feature\Blog\Like\Action\ReplyLiking;

final class ReplyLikingGuard implements ActionGuardInterface
{
    public function __invoke(ReplyLiking $action, FeedbackInterface $feedback): void
    {
        $reply = $action->reply;
        if ($reply->isLikedBy($action->user)) {
            $feedback->denyBecause('Reply is already liked by user');
        }
    }
}
