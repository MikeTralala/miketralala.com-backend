<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Like\Action\PostLiking;

final class PostLikingGuard implements ActionGuardInterface
{
    public function __invoke(PostLiking $action, FeedbackInterface $feedback): void
    {
        $post = $action->post;
        if ($post->isLikedBy($action->user)) {
            $feedback->denyBecause('Post is already liked by user');
        }
    }
}
