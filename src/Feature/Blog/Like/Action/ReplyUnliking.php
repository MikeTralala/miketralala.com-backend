<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Action;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\User\Entity\User;

final class ReplyUnliking
{
    public function __construct(
        public readonly Reply $reply,
        public readonly User $user
    ) {
    }
}
