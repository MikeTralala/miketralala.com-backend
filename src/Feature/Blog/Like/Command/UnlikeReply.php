<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class UnlikeReply
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $replyId,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
    ) {
    }
}
