<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Command;
use Symfony\Component\Validator\Constraints as Assert;

final class UnlikePost
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $postId,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
    ) {
    }
}
