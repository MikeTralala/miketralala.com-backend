<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Like\Action\PostLiking;
use App\Feature\Blog\Like\Command\LikePost;
use App\Feature\Blog\Like\Entity\Like;
use App\Feature\Blog\Like\Event\PostLiked;
use App\Feature\Blog\Like\Repository\LikeRepository;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\User\Repository\UserRepository;
use DateTimeImmutable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;
use Symfony\Component\Uid\Ulid;

final class LikePostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private LikeRepository $likeRepository,
        private PostRepository $postRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(LikePost $command): void
    {
        $post = $this->postRepository->mustFind($command->postId);
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new PostLiking($post, $user));

        $like = new Like(
            Ulid::generate(),
            $user,
            new DateTimeImmutable()
        );

        $this->likeRepository->save($like);

        $this->eventBus->dispatch(new PostLiked($like, $post), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
