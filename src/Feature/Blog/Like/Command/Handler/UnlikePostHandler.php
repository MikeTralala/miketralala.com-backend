<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Like\Action\PostUnliking;
use App\Feature\Blog\Like\Command\LikePost;
use App\Feature\Blog\Like\Event\PostLiked;
use App\Feature\Blog\Like\Event\PostUnliked;
use App\Feature\Blog\Like\Repository\LikeRepository;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class LikePostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private LikeRepository $likeRepository,
        private PostRepository $postRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(LikePost $command): void
    {
        $post = $this->postRepository->mustFind($command->postId);
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new PostUnliking($post, $user));

        $like = $post->getUserLike($user);
        if (null !== $like) {
            $this->likeRepository->remove($like);
        }

        $this->eventBus->dispatch(new PostUnliked($like->getId(), $post), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
