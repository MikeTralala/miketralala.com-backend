<?php

declare(strict_types=1);

namespace App\Feature\Blog\Like\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Like\Action\ReplyLiking;
use App\Feature\Blog\Like\Command\LikeReply;
use App\Feature\Blog\Like\Event\ReplyUnliked;
use App\Feature\Blog\Like\Repository\LikeRepository;
use App\Feature\Blog\Reply\Repository\ReplyRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class UnlikeReplyHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private LikeRepository $likeRepository,
        private ReplyRepository $replyRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(LikeReply $command): void
    {
        $reply = $this->replyRepository->mustFind($command->replyId);
        $user  = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new ReplyLiking($reply, $user));

        $like = $reply->getUserLike($user);
        if (null !== $like) {
            $this->likeRepository->remove($like);
        }

        $this->eventBus->dispatch(new ReplyUnliked($like->getId(), $reply), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
