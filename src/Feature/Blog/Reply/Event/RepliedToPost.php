<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Event;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\Blog\Reply\Entity\Reply;

final class RepliedToPost
{
    public function __construct(
        public readonly Reply $reply,
        public readonly Post $post
    ) {
    }
}
