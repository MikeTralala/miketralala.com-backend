<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Event;

use App\Feature\User\Entity\User;
use App\Feature\Blog\Reply\Entity\Reply;

final class RepliedToReply
{
    public function __construct(
        public readonly Reply $reply,
        public readonly Reply $parentReply
    ) {
    }
}
