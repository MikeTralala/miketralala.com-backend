<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Event;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\User\Entity\User;
use App\Feature\Blog\Reply\Entity\Reply;

final class ReplyRemoved
{
    public function __construct(
        public readonly string $replyId
    ) {
    }
}
