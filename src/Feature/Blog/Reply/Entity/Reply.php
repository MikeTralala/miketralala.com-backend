<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Entity;

use App\Feature\Blog\Like\Entity\Like;
use App\Feature\Blog\Post\Entity\Post;
use App\Feature\User\Entity\User;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Reply
{
    public const MAX_REPLIES = 99;

    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\Column(type: 'text', nullable: false)]
    private string $content;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\OneToMany(targetEntity: Like::class, mappedBy: 'like', cascade: ['persist', 'remove'])]
    private Collection $likes;

    #[ORM\OneToMany(targetEntity: Reply::class, mappedBy: 'parentReply', cascade: ['persist', 'remove'])]
    private Collection $replies;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private DateTimeImmutable $lastUpdatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $lastUpdatedBy;

    #[ORM\ManyToOne(targetEntity: Post::class, inversedBy: 'replies')]
    private ?Post $post = null;

    #[ORM\ManyToOne(targetEntity: Reply::class, inversedBy: 'replies')]
    private ?Reply $parentReply = null;

    public function __construct(
        string $id,
        User $author,
        Post $post,
        string $content
    ) {
        $this->replies = new ArrayCollection();
        $this->likes   = new ArrayCollection();

        $this->id            = $id;
        $this->author        = $author;
        $this->post          = $post;
        $this->content       = $content;
        $this->at            = new DateTimeImmutable();
        $this->lastUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeImmutable
    {
        return $this->at;
    }

    public function getLike(Criteria $criteria): ?Like
    {
        return $this->getLikes($criteria)->first() ?: null;
    }

    public function getLikes(?Criteria $criteria = null): Collection
    {
        if (null === $criteria) {
            return $this->likes;
        }

        return $this->likes->matching($criteria);
    }

    public function addLike(Like $like): void
    {
        $like->setReply($this);
        $this->likes->add($like);
    }

    public function removeLike(Like $like): void
    {
        $this->likes->removeElement($like);
    }

    public function getReplies(?Criteria $criteria = null): Collection
    {
        if (null === $criteria) {
            return $this->replies;
        }

        return $this->replies->matching($criteria);
    }

    public function addReply(Reply $reply): void
    {
        $reply->setParentReply($this);
        $this->replies->add($reply);
    }

    public function removeReply(Reply $reply): void
    {
        $this->replies->removeElement($reply);
    }

    public function getLastUpdatedAt(): DateTimeImmutable
    {
        return $this->lastUpdatedAt;
    }

    public function getLastUpdatedBy(): ?User
    {
        return $this->lastUpdatedBy;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content, User $user): void
    {
        $this->content = $content;
        $this->updated($user);
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function getParentReply(): ?Reply
    {
        return $this->parentReply;
    }

    public function setParentReply(Reply $parentReply): void
    {
        $this->parentReply = $parentReply;
    }

    public function getOriginalReply(): self
    {
        $parent = $this->getParentReply();
        if (null === $parent) {
            return $this;
        }

        return $parent->getOriginalReply();
    }

    private function updated(User $user): void
    {
        $this->lastUpdatedAt = new DateTimeImmutable();
        $this->lastUpdatedBy = $user;
    }

    public function isLikedBy(User $user): bool
    {
        return $this->likes->exists(function (Like $like) use ($user) {
            return $like->getBy()->is($user);
        });
    }

    public function getUserLike(User $user):?Like
    {
        return $this->likes->filter(function (Like $like) use ($user) {
            return $like->getBy()->is($user);
        })->first();
    }
}
