<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Repository;

use App\Exception\AppRuntimeException;
use App\Feature\Blog\Reply\Entity\Reply;
use App\Repository\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reply|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reply|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reply[]    findAll()
 * @method Reply[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReplyRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: Reply::class
        );
    }

    public function mustFind(string $id): Reply
    {
        $reply = $this->find($id);
        if (null === $reply) {
            throw AppRuntimeException::notFound(Reply::class, $id);
        }

        return $reply;
    }
}
