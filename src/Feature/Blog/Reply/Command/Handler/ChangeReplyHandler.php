<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Reply\Action\ReplyAlteration;
use App\Feature\Blog\Reply\Command\ChangeReply;
use App\Feature\Blog\Reply\Event\ReplyChanged;
use App\Feature\Blog\Reply\Repository\ReplyRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ChangeReplyHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private ReplyRepository $replyRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(ChangeReply $command): void
    {
        $reply = $this->replyRepository->mustFind($command->replyId);
        $user  = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new ReplyAlteration($reply, $user));

        $reply->setContent($command->content, $user);

        $this->eventBus->dispatch(new ReplyChanged($reply), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}

