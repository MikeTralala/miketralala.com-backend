<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Reply\Action\ReplyingToReply;
use App\Feature\Blog\Reply\Command\ReplyToReply;
use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\Blog\Reply\Event\RepliedToReply;
use App\Feature\Blog\Reply\Repository\ReplyRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ReplyToReplyHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private ReplyRepository $replyRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(ReplyToReply $command): void
    {
        $parentReply = $this->replyRepository->mustFind($command->parentReplyId);
        $author      = $this->userRepository->mustFind($command->authorId);

        $this->guardian->protect(new ReplyingToReply($parentReply, $author));

        $reply = new Reply(
            $command->replyId,
            $author,
            $parentReply->getPost(),
            $command->content
        );

        $reply->setParentReply($parentReply);

        $this->replyRepository->save($reply);

        $this->eventBus->dispatch(new RepliedToReply($reply, $parentReply), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
