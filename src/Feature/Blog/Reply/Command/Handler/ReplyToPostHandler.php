<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\Blog\Reply\Action\ReplyingToPost;
use App\Feature\Blog\Reply\Command\ReplyToPost;
use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\Blog\Reply\Event\RepliedToPost;
use App\Feature\Blog\Reply\Repository\ReplyRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ReplyToPostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private ReplyRepository $replyRepository,
        private PostRepository $postRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(ReplyToPost $command): void
    {
        $post   = $this->postRepository->mustFind($command->postId);
        $author = $this->userRepository->mustFind($command->authorId);

        $this->guardian->protect(new ReplyingToPost($post, $author));

        $reply = new Reply(
            $command->replyId,
            $author,
            $post,
            $command->content
        );

        $this->replyRepository->save($reply);

        $this->eventBus->dispatch(new RepliedToPost($reply, $post), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
