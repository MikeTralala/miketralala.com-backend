<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Reply\Action\ReplyRemoval;
use App\Feature\Blog\Reply\Command\RemoveReply;
use App\Feature\Blog\Reply\Event\ReplyRemoved;
use App\Feature\Blog\Reply\Repository\ReplyRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class RemoveReplyHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private ReplyRepository $replyRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(RemoveReply $command): void
    {
        $reply = $this->replyRepository->mustFind($command->replyId);
        $user  = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new ReplyRemoval($reply, $user));

        $this->replyRepository->remove($reply);

        $this->eventBus->dispatch(new ReplyRemoved($reply->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
