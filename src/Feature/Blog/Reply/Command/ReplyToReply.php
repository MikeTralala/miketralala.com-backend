<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class ReplyToReply
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $replyId,
        #[Assert\NotBlank]
        public readonly string $content,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $authorId,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $parentReplyId,
    ) {
    }
}
