<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Post\Entity\Post;
use App\Feature\Blog\Reply\Action\ReplyingToPost;

final class ReplyingToPostGuard implements ActionGuardInterface
{
    public function __invoke(ReplyingToPost $action, FeedbackInterface $feedback): void
    {
        $post = $action->post;
        if ($post->getReplies()->count() > Post::MAX_REPLIES) {
            $feedback->denyBecause('Maximum amount of replies is reached');
        }
    }
}
