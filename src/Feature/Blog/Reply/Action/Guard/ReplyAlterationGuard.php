<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Reply\Action\ReplyAlteration;
use App\Feature\User\Entity\Role;

final class ReplyAlterationGuard implements ActionGuardInterface
{
    public function __invoke(ReplyAlteration $action, FeedbackInterface $feedback): void
    {
        $reply = $action->reply;
        $user  = $action->user;

        if ($user->hasRole(Role::MODERATOR) || $user->is($reply->getAuthor())) {
            return;
        }

        $feedback->denyBecause('Only the reply author or a moderator can change a post');
    }
}
