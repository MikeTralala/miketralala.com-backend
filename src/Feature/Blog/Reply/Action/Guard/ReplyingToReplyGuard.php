<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Reply\Action\ReplyingToReply;
use App\Feature\Blog\Reply\Entity\Reply;

final class ReplyingToReplyGuard implements ActionGuardInterface
{
    public function __invoke(ReplyingToReply $action, FeedbackInterface $feedback): void
    {
        $reply = $action->reply;
        if ($reply->getReplies()->count() > Reply::MAX_REPLIES) {
            $feedback->denyBecause('Maximum amount of replies is reached');
        }
    }
}
