<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Action;

use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\User\Entity\User;

final class ReplyingToReply
{
    public function __construct(
        public readonly Reply $reply,
        public readonly User $user
    ) {
    }
}
