<?php

declare(strict_types=1);

namespace App\Feature\Blog\Reply\Action;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\User\Entity\User;

final class ReplyingToPost
{
    public function __construct(
        public readonly Post $post,
        public readonly User $user
    ) {
    }

}
