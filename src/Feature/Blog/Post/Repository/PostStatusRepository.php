<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Repository;

use App\Exception\AppRuntimeException;
use App\Feature\Blog\Post\Entity\PostStatus;
use App\Repository\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostStatus[]    findAll()
 * @method PostStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostStatusRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: PostStatus::class
        );
    }

    public function mustFind(int $id): PostStatus
    {
        $postStatus = $this->find($id);
        if (null === $postStatus) {
            throw AppRuntimeException::notFound(PostStatus::class, $id);
        }

        return $postStatus;
    }
}
