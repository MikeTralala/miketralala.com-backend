<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Repository;

use App\Exception\AppRuntimeException;
use App\Feature\Blog\Post\Entity\Post;
use App\Repository\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: Post::class
        );
    }

    public function mustFind(string $id): Post
    {
        $post = $this->find($id);
        if (null === $post) {
            throw AppRuntimeException::notFound(Post::class, $id);
        }

        return $post;
    }

    public function findOneBySlug(string $slug): ?Post
    {
        $query = $this->createQueryBuilder('post')
                      ->where('post.slug = :slug')->setParameter('slug', $slug)
                      ->getQuery();

        return $query->setMaxResults(1)
                     ->getOneOrNullResult();
    }
}
