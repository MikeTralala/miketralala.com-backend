<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Event;

use App\Feature\Blog\Post\Entity\Post;

final class PostPublished
{
    public function __construct(
        public readonly Post $post
    ) {
    }
}
