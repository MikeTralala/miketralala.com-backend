<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Event;

final class PostRemoved
{
    public function __construct(
        public readonly string $postId
    ) {
    }
}
