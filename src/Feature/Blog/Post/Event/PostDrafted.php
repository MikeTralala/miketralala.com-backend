<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Event;

use App\Feature\Blog\Post\Entity\Post;

final class PostDrafted
{
    public function __construct(
        public readonly Post $post
    ) {
    }
}
