<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class PublishPost
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $postId,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId
    ) {
    }
}
