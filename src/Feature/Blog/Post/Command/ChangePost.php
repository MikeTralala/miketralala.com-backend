<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class ChangePost
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $postId,
        #[Assert\NotBlank]
        public readonly string $title,
        #[Assert\NotBlank]
        public readonly string $content,
        #[Assert\NotBlank]
        public readonly string $image,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId
    ) {
    }
}
