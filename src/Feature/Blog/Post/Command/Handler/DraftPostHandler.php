<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Post\Action\PostCreation;
use App\Feature\Blog\Post\Command\DraftPost;
use App\Feature\Blog\Post\Entity\Post;
use App\Feature\Blog\Post\Entity\PostState;
use App\Feature\Blog\Post\Entity\PostStatus;
use App\Feature\Blog\Post\Event\PostDrafted;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\Blog\Post\Repository\PostStatusRepository;
use App\Feature\User\Repository\UserRepository;
use App\Helper\Utilities;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class DraftPostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private PostRepository $postRepository,
        private PostStatusRepository $postStatusRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(DraftPost $command): void
    {
        $author = $this->userRepository->mustFind($command->authorId);

        $this->guardian->protect(new PostCreation($author));

        $post = new Post(
            $command->postId,
            $command->title,
            $this->createUniqueSlug($command->title),
            $command->content,
            $command->image,
            new PostState(
                $this->postStatusRepository->mustFind(PostStatus::DRAFT),
                $author
            )
        );

        $this->postRepository->save($post);

        $this->eventBus->dispatch(new PostDrafted($post), [
            new DispatchAfterCurrentBusStamp(), // TODO: Can be default
        ]);
    }

    private function createUniqueSlug(string $title, ?int $iteration = null): string
    {
        $slug = $this->generateSlug($title, $iteration);

        if (null === $this->postRepository->findOneBySlug($slug)) {
            return trim($slug);
        }

        return $this->createUniqueSlug($title, ++$iteration);
    }

    private function generateSlug(string $title, ?int $iteration): string
    {
        $slug = Utilities::slugify($title);

        if (null === $iteration) {
            return $slug;
        }

        return sprintf('%s-%s', $slug, $iteration);
    }
}
