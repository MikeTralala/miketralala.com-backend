<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Post\Action\PostAlteration;
use App\Feature\Blog\Post\Command\ChangePost;
use App\Feature\Blog\Post\Event\PostChanged;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ChangePostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private PostRepository $postRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(ChangePost $command): void
    {
        $post = $this->postRepository->mustFind($command->postId);
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new PostAlteration($post, $user));

        $post->setTitle($command->title, $user);
        $post->setImage($command->image, $user);
        $post->setContent($command->content, $user);

        $this->postRepository->save($post);

        $this->eventBus->dispatch(new PostChanged($post), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
