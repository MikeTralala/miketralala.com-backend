<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Post\Action\PostPublication;
use App\Feature\Blog\Post\Command\DraftPost;
use App\Feature\Blog\Post\Command\PublishPost;
use App\Feature\Blog\Post\Entity\PostState;
use App\Feature\Blog\Post\Entity\PostStatus;
use App\Feature\Blog\Post\Event\PostPublished;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\Blog\Post\Repository\PostStatusRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class PublishPostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private PostRepository $postRepository,
        private PostStatusRepository $postStatusRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(PublishPost $command): void
    {
        $post = $this->postRepository->mustFind($command->postId);
        $user = $this->userRepository->mustFind($command->userId);

        $this->guardian->protect(new PostPublication($post, $user));

        $post->addState(
            new PostState(
                $this->postStatusRepository->mustFind(PostStatus::PUBLISHED),
                $user
            )
        );

        $this->postRepository->save($post);

        $this->eventBus->dispatch(new PostPublished($post), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }

}
