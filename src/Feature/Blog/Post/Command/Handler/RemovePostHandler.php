<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Command\Handler;

use App\ActionGuard\GuardianInterface;
use App\Feature\Blog\Post\Action\PostRemoval;
use App\Feature\Blog\Post\Command\DraftPost;
use App\Feature\Blog\Post\Entity\PostState;
use App\Feature\Blog\Post\Entity\PostStatus;
use App\Feature\Blog\Post\Event\PostRemoved;
use App\Feature\Blog\Post\Repository\PostRepository;
use App\Feature\Blog\Post\Repository\PostStatusRepository;
use App\Feature\User\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class RemovePostHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private PostRepository $postRepository,
        private PostStatusRepository $postStatusRepository,
        private MessageBusInterface $eventBus,
        private GuardianInterface $guardian
    ) {
    }

    public function __invoke(DraftPost $command): void
    {
        $post = $this->postRepository->mustFind($command->postId);
        $user = $this->userRepository->mustFind($command->authorId);

        $this->guardian->protect(new PostRemoval($post, $user));

        $post->addState(
            new PostState(
                $this->postStatusRepository->mustFind(PostStatus::REMOVED),
                $user
            )
        );

        $this->eventBus->dispatch(new PostRemoved($post->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }

}
