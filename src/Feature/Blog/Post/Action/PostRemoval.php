<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action;

use App\Feature\Blog\Post\Entity\Post;
use App\Feature\User\Entity\User;

final class PostRemoval
{
    public function __construct(
        public readonly Post $post,
        public readonly User $user
    ) {
    }
}
