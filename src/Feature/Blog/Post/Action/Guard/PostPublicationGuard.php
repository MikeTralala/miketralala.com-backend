<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Post\Action\PostPublication;
use App\Feature\Blog\Post\Entity\PostStatus;

final class PostPublicationGuard implements ActionGuardInterface
{
    public function __invoke(PostPublication $action, FeedbackInterface $feedback): void
    {
        $user = $action->user;
        $post = $action->post;

        if ($post->hasState(PostStatus::REMOVED)) {
            $feedback->denyBecause('Removed posts can not be published again');
        }

        if ($post->hasState(PostStatus::PUBLISHED)) {
            $feedback->denyBecause('Post is already published');
        }

        if (! $user->is($post->getAuthor())) {
            $feedback->denyBecause('Only the author of the post can publish a post');
        }
    }
}
