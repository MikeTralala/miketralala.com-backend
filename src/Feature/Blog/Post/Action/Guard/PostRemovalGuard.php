<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Post\Action\PostAlteration;
use App\Feature\Blog\Post\Entity\PostStatus;
use App\Feature\User\Entity\Role;

final class PostRemovalGuard implements ActionGuardInterface
{
    public function __invoke(PostAlteration $action, FeedbackInterface $feedback): void
    {
        $post = $action->post;
        if ($post->hasState(PostStatus::REMOVED)) {
            $feedback->denyBecause('Post is already removed');
        }

        $user = $action->user;
        if ($user->hasRole(Role::MODERATOR) || $user->is($post->getAuthor())) {
            return;
        }

        $feedback->denyBecause('Only the post author or a moderator can remove a post');
    }
}
