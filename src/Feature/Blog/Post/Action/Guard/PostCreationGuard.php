<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action\Guard;

use App\ActionGuard\ActionGuardInterface;
use App\ActionGuard\FeedbackInterface;
use App\Feature\Blog\Post\Action\PostCreation;
use App\Feature\User\Entity\Role;

final class PostCreationGuard implements ActionGuardInterface
{
    public function __invoke(PostCreation $action, FeedbackInterface $feedback): void
    {
        $user = $action->user;
        if (! $user->hasRole(Role::AUTHOR)) {
            $feedback->denyBecause('Only authors can create posts');
        }
    }
}
