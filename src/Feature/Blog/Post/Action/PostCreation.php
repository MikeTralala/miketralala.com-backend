<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action;

use App\Feature\User\Entity\User;

final class PostCreation
{
    public function __construct(
        public readonly User $user
    ) {
    }
}
