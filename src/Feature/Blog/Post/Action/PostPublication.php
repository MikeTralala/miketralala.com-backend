<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Action;

use App\Feature\User\Entity\User;
use App\Feature\Blog\Post\Entity\Post;

final class PostPublication
{
    public function __construct(
        public readonly Post $post,
        public readonly User $user
    ) {
    }
}
