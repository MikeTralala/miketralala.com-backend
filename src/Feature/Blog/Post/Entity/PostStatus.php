<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Entity;

use App\Entity\Traits\HasComparableId;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PostStatus
{
    use HasComparableId;

    final public const DRAFT     = 1;
    final public const PUBLISHED = 2;
    final public const REMOVED   = 3;

    #[ORM\Id]
    #[ORM\Column(type: 'integer', nullable: false)]
    private int    $id;
    #[ORM\Column(type: 'string', nullable: false)]
    private string $name;
    #[ORM\Column(type: 'string', nullable: false)]
    private string $code;

    public function __construct(
        int $id,
        string $name
    ) {
        $this->id   = $id;
        $this->name = $name;
        $this->code = strtoupper($name);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
