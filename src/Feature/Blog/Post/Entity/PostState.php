<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Entity;

use App\Feature\User\Entity\User;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
class PostState
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: PostStatus::class)]
    private PostStatus $status;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private User $user;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\ManyToOne(targetEntity: Post::class, inversedBy: 'states')]
    private ?Post $post;

    public function __construct(
        PostStatus $status,
        User $user
    ) {
        $this->id     = Ulid::generate();
        $this->status = $status;
        $this->user   = $user;
        $this->at     = new DateTimeImmutable();
    }

    public function setBlogPost(Post $post): void
    {
        $this->post = $post;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeImmutable|DateTimeInterface
    {
        return $this->at;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function getStatus(): PostStatus
    {
        return $this->status;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
