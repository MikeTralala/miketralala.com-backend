<?php

declare(strict_types=1);

namespace App\Feature\Blog\Post\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Feature\Blog\Like\Entity\Like;
use App\Feature\Blog\Reply\Entity\Reply;
use App\Feature\User\Entity\User;
use App\Resolver\AddBlogPostReplyToPostResolver;
use App\Resolver\AddBlogPostResolver;
use App\Resolver\ChangeBlogPostResolver;
use App\Resolver\LikeBlogPostResolver;
use App\Resolver\PublishBlogPostResolver;
use App\Resolver\RemoveBlogPostResolver;
use App\Resolver\UnlikeBlogPostResolver;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Post
{
    public const MAX_REPLIES = 999;

    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\Column(type: 'string')]
    private string $title;

    #[ORM\Column(type: 'string')]
    private string $slug;

    #[ORM\Column(type: 'text')]
    private string $content;

    #[ORM\Column(type: 'string')]
    private string $image;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $lastUpdatedBy;

    #[ORM\ManyToOne(targetEntity: PostStatus::class)]
    #[ORM\JoinColumn(nullable: false)]
    private PostStatus $status;

    #[ORM\OneToMany(targetEntity: PostState::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    private Collection $states;

    #[ORM\OneToMany(targetEntity: Like::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    private Collection $likes;

    #[ORM\OneToMany(targetEntity: Reply::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    private Collection $replies;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $publishedAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private DateTimeImmutable $lastUpdatedAt;

    public function __construct(
        string $id,
        string $title,
        string $slug,
        string $content,
        string $image,
        PostState $state
    ) {
        $this->states  = new ArrayCollection();
        $this->replies = new ArrayCollection();
        $this->likes   = new ArrayCollection();

        $this->id            = $id;
        $this->title         = trim($title);
        $this->slug          = trim($slug);
        $this->content       = $content;
        $this->image         = $image;
        $this->author        = $state->getUser();
        $this->lastUpdatedAt = new DateTimeImmutable();
        $this->addState($state);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getStatus(): PostStatus
    {
        return $this->status;
    }

    public function addState(PostState $state): void
    {
        $state->setBlogPost($this);
        $this->status = $state->getStatus();
        $this->states->add($state);

        if ($state->getStatus()->is(PostStatus::PUBLISHED)) {
            $this->publishedAt = $state->getAt();
        }
    }

    public function getStates(): Collection
    {
        return $this->states;
    }

    public function getLike(Criteria $criteria): ?Like
    {
        return $this->getLikes($criteria)->first() ?: null;
    }

    public function getLikes(?Criteria $criteria = null): Collection
    {
        if (null === $criteria) {
            return $this->likes;
        }

        return $this->likes->matching($criteria);
    }

    public function addLike(Like $like): void
    {
        $like->setPost($this);
        $this->likes->add($like);
    }

    public function removeLike(Like $like): void
    {
        $this->likes->removeElement($like);
    }

    public function getReplies(?Criteria $criteria = null): Collection
    {
        if (null === $criteria) {
            return $this->replies;
        }

        return $this->replies->matching($criteria);
    }

    public function addReply(Reply $reply): void
    {
        $reply->setPost($this);
        $this->replies->add($reply);
    }

    public function removeReply(Reply $reply): void
    {
        $this->replies->removeElement($reply);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title, User $user): void
    {
        if ($this->title === $title) {
            return;
        }

        $this->updated($user);
        $this->title = $title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content, User $user): void
    {
        if ($this->content === $content) {
            return;
        }

        $this->updated($user);
        $this->content = $content;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image, User $user): void
    {
        if ($this->image === $image) {
            return;
        }

        $this->updated($user);
        $this->image = $image;
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function getLastUpdatedAt(): DateTimeImmutable
    {
        return $this->lastUpdatedAt;
    }

    public function getLastUpdatedBy(): ?User
    {
        return $this->lastUpdatedBy;
    }

    private function updated(User $user): void
    {
        $this->lastUpdatedBy = $user;
        $this->lastUpdatedAt = new DateTimeImmutable();
    }

    public function hasState(PostStatus|int|array $status): bool
    {
        return $this->states->exists(function (PostState $state) use ($status) {
            return $state->getStatus()->is($status);
        });
    }

    public function isLikedBy(User $user): bool
    {
        return $this->likes->exists(function (Like $like) use ($user) {
            return $like->getBy()->is($user);
        });
    }

    public function getUserLike(User $user):?Like
    {
        return $this->likes->filter(function (Like $like) use ($user) {
            return $like->getBy()->is($user);
        })->first();
    }
}
