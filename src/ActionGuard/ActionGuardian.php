<?php

declare(strict_types=1);

namespace App\ActionGuard;

use App\ActionGuard\Strategy\StrategyFeedbackInterface;
use App\ActionGuard\Strategy\UnanimousStrategyFeedback;
use App\Exception\AppRuntimeException;
use Psr\Log\LoggerInterface;
use ReflectionClass;

final class ActionGuardian implements GuardianInterface
{
    /**
     * @var array|FeedbackInterface[][]
     */
    private array $guards;

    public function __construct(
        private LoggerInterface $logger,
        iterable $guards
    ) {
        foreach ($guards as $guard) {
            if (! $guard instanceof ActionGuardInterface) {
                continue;
            }

            $reflectedClass = new ReflectionClass($guard);
            if ($reflectedClass->hasMethod('__invoke')) {
                continue;
            }

            $actionClass = $reflectedClass?->getMethod('__invoke')->getParameters()[0]?->getType()?->getName();
            if (null === $actionClass) {
                continue;
            }

            $this->guards[$actionClass][] = $guard;
        }
    }

    public function protect(object|array $action): void
    {
        if (! is_array($action)) {
            $feedback = $this->collectFeedbackFor($action);

            $this->processFeedback($action, $feedback);
        }

        array_map([$this, 'protect'], array_filter($action));
    }

    public function canAllow(object|array $action): bool
    {
        try {
            $this->protect($action);
        } catch (AppRuntimeException) {
            return false;
        }

        return true;
    }

    private function collectFeedbackFor(object $action): StrategyFeedbackInterface
    {
        $feedback = new Feedback();

        $guards = $this->guards[get_class($action)];
        foreach ($guards as $guard) {
            $guard($action, $feedback);
        }

        $strategyClass = UnanimousStrategyFeedback::class;
        if ($action instanceof StrategicActionInterface) {
            $strategyClass = $action::getStrategyClass();

            if (! is_subclass_of($strategyClass, StrategyFeedbackInterface::class)) {
                throw new \LogicException();
            }
        }

        return new $strategyClass($feedback);
    }

    private function processFeedback(object $action, StrategyFeedbackInterface $feedback): void
    {
        if ($feedback->isActionApproved()) {
            $this->logger->debug('Action {action} can proceed', [
                'action'    => get_class($action),
                'denials'   => $feedback->getNegotiation()->getAllDenials(),
                'approvals' => $feedback->getNegotiation()->getAllApprovals(),
            ]);

            return;
        }

        $this->logger->debug('Execution of action {action} prevented', [
            'action'    => get_class($action),
            'denials'   => $feedback->getNegotiation()->getAllDenials(),
            'approvals' => $feedback->getNegotiation()->getAllApprovals(),
        ]);

        throw new AppRuntimeException($feedback->getDenialReason());
    }

}
