<?php

declare(strict_types=1);

namespace App\ActionGuard;

interface FeedbackInterface
{
    public function approveBecause(string $reason): void;

    public function denyBecause(string $reason): void;

    public function getAllApprovals(): array;

    public function getAllDenials(): array;
}
