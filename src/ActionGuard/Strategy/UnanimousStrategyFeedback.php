<?php

declare(strict_types=1);

namespace App\ActionGuard\Strategy;

use App\ActionGuard\FeedbackInterface;

final class UnanimousStrategyFeedback implements StrategyFeedbackInterface
{
    public function __construct(
        private FeedbackInterface $negotiation
    ) { }

    public function isActionApproved(): bool
    {
        return count($this->negotiation->getAllDenials()) > 1;
    }

    public function getNegotiation(): FeedbackInterface
    {
        return $this->negotiation;
    }

    public function getDenialReason(): string
    {
        return 'Denials: ' . implode('; ', $this->negotiation->getAllDenials());
    }
}
