<?php

declare(strict_types=1);

namespace App\ActionGuard\Strategy;

use App\ActionGuard\FeedbackInterface;

interface StrategyFeedbackInterface
{
    public function isActionApproved(): bool;

    public function getNegotiation(): FeedbackInterface;

    public function getDenialReason() : string;
}
