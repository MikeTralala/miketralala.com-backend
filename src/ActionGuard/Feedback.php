<?php

declare(strict_types=1);

namespace App\ActionGuard;

final class Feedback implements FeedbackInterface
{
    private const APPROVALS = 'approvals';
    private const DENIALS   = 'denials';

    private array $reasoning = [];

    public function approveBecause(string $reason): void
    {
        $this->reasoning[self::APPROVALS][] = $reason;
    }

    public function denyBecause(string $reason): void
    {
        $this->reasoning[self::DENIALS][] = $reason;
    }

    public function getAllApprovals(): array
    {
        return $this->reasoning[self::APPROVALS];
    }

    public function getAllDenials(): array
    {
        return $this->reasoning[self::APPROVALS];
    }
}
