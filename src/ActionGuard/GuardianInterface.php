<?php

declare(strict_types=1);

namespace App\ActionGuard;

interface GuardianInterface
{
    public function protect(object $action): void;
}
