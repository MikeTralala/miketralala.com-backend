<?php

declare(strict_types=1);

namespace App\ActionGuard;

interface StrategicActionInterface
{
    public static function getStrategyClass(): string;
}
